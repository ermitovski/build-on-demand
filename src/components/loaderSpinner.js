import React, { Component } from "react";

export default class LoaderSpinner extends Component {
  render() {
    return (
      <div className="container loader-spinner">
        <div className="lds-grid">
          <div></div>
          <div></div>
          <div></div>
          <div></div>
          <div></div>
          <div></div>
          <div></div>
          <div></div>
          <div></div>
        </div>
        <p>{this.props.loadingMessage}</p>
      </div>
    );
  }
}
